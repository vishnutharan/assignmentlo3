package Assignment_Package_Task4;

import java.util.Scanner;

public class LoginUser
{

    private String LoginId;
    private String Password;

    private String Id;
    private String Address;
    private String Phone;
    private String Email;

    private void getLoginInfo()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter user Id \n");
        this.LoginId = scanner.next();
        System.out.println("Enter your password \n");
        this.Password = scanner.next();

        nextStep(check());

    }

    private boolean check()
    {
        UserDetail userDetail = new UserDetail();
        userDetail.setDetails();
        this.Id = userDetail.Id;
        this.Address = userDetail.Address;
        this.Phone = userDetail.phone;
        this.Email = userDetail.Email;

        boolean checking = userDetail.runUserId(this.LoginId,this.Password);
        if(checking)
        {
            return true;

        }
        else
        {
            return false;
        }

    }

    private void nextStep(boolean check)
    {
        if(check())
        {
            System.out.println("1.Show my profile");
            System.out.println("2.Product page");
            Scanner scanner = new Scanner(System.in);
            int ans = scanner.nextInt();

            if(ans==1)
            {
                Customer_Details customer = new Customer_Details();
                customer.runAccount(this.Id, this.Address, this.Phone, this.Email);
            }

            else if (ans==2)
            {
                Products product = new Products();
                product.runProduct();
            }

            else
            {
                System.out.println("Enter correct number");
            }
        }
        else
        {
            System.out.println("User name or Password Wrong");
        }
    }

    public void runLogin()
    {
        getLoginInfo();

    }
}
