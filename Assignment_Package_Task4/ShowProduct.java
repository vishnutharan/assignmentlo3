package Assignment_Package_Task4;

import java.util.Scanner;

public class ShowProduct
{
    private String productName;
    private String material ;
    private String brand;
    private int price;
    Scanner scanner = new Scanner(System.in);
    protected void cloths()
    {

        System.out.println("\n1.Shirt	\n" +
                            "2.jeans	\n" +
                            "3.jacket	\n\n " +
                            "Which cloths you need to buy?");


        int ans = scanner.nextInt();

        switch (ans)
        {
            case 1:
                    this.productName= "Shirt";
                    this.material   = "Cotton";
                    this.brand      = "signature";
                    this.price      =  1500;
                break;
            case 2:
                    this.productName= "jeans ";
                    this.material   = "Leather";
                    this.brand      = "Levi's.";
                    this.price      =  3900;
                break;
            case 3:
                    this.productName = "jacket";
                    this.material    = "Leather";
                    this.brand      =  "Belstaff";
                    this.price		 =  5000;
                break;
            default:
                System.out.println("Your answer is wrong");
                break;

        }
        showProduct("cloths");
    }

// next
 
    protected void Phone()
    {

        System.out.println("\n1.IPhone\n" +
                "2.Samsung\n" +
                "3.Nokia \n\n " +
                "Which Phone you need to buy?");


        int ans = scanner.nextInt();

        switch (ans)
        {
            case 1:
                this.productName= "IPHONE";
                this.price= 200000;
                break;
            case 2:
                this.productName= "Samsung";
                this.price= 100000;
                break;
            case 3:
                this.productName= "Nokia";
                this.price= 50000;
                break;
            default:
                System.out.println("Your answer is wrong");
                break;

        }
        showProduct("Phone");
    }

    //next

    protected void computer()
    {

        System.out.println("\n1.Dell\n" + 
                			"2.Hp \n" +
                			"3.Mac Book \n\n " +
                			"Which Computer you need to buy?");


        int ans = scanner.nextInt();

        switch (ans)
        {
            case 1:
                this.productName= "Dell";
                this.price= 90000;
                break;
            case 2:
                this.productName= "Hp";
                this.price= 100000;
                break;
            case 3:
                this.productName= "Mac Book";
                this.price= 300000;
                break;
            default:
                System.out.println("Your answer is wrong");
                break;

        }
        showProduct("computer");
    }

    private void showProduct(String product)
    {
        System.out.println("\nProduct name :" + this.productName + "\nprice :" + this.price + "Rs" + "\n\nEnter \"B\" for buy the product"+"\nEnter \"C\" for back");
        String ans = scanner.next().toLowerCase();
        switch (ans)
        {
            case "b":
                BuyPage buyPage = new BuyPage();
                buyPage.runBuy(this.productName,this.price);
                break;
            case "c":
                switch (product)
                {
                    case "cloths":
                        cloths();
                        break;
                    case "Phone":
                    	Phone();
                        break;
                    case "computer":
                    	computer();
                        break;
                    default:
                        System.out.println("Your answer is wrong");
                        break;

                }
                break;
            default:

                System.out.println("Your answer is wrong");
                break;
        }

    }


}
